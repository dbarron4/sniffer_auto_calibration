"""     Copyright (c) 2018. Treetown Tech LLC     """

# !/usr/bin/env python3

import os
import time
import numpy
import struct
import threading
from datetime import datetime
from boltons.socketutils import BufferedSocket
import traceback

# import automationhat
from connectBT import connectBT
from mutables import MutableTimer, Timer

lock = threading.Lock()

CAL_TRIAL_TIMER = 30
NUM_TRIALS = 3
NUM_VALVES_ACTIVE = 2       # Zero and Methane      # Max 3 outputs

VALVE_TO_GAS_MAP = {
    0: 'ZERO',
    1: 'CH4'
}

GAS_CONCENTRATIONS = {
    # 'ZERO': 0,
    'ZERO': 500,        # TODO: Change
    'CH4': 500,
}


def create_csv():
    name = "testfile"
    extension = ".csv"
    fileName = name + extension
    x = 0
    while (os.path.exists(fileName) != False):
        x = x + 1
        fileName = name + str(x) + extension
    file = open(fileName, 'w+')
    print(name + str(x) + extension)
    file.write("Date, Time, 92000707, 92000709, Timer")
    file.write('\n')
    return file


"""
    Timer Flag - mutex
    RGB Code    
"""


def parseCalData(data_dict, instrument_id):
    gas = VALVE_TO_GAS_MAP[1]           # TODO: Change index to argument = pin/valve number
    conc = GAS_CONCENTRATIONS[gas]
    conc_90 = conc * 0.9

    date = str(data_dict[1]['ZERO']['start_timestamp'].date())
    trial_time_list = []
    zero_gas_meter_reading_list = []
    cal_gas_meter_reading_list = []
    diff_reading_list = []

    for i in range(1, NUM_TRIALS+1):
        trial_time_list.append(data_dict[i][gas]['time_at_gas_90'])
        zero_gas_meter_reading_list.append(data_dict[i]['ZERO']['gas_value'])
        cal_gas_meter_reading_list.append(data_dict[i][gas]['gas_value'])
        diff_reading_list.append(conc - data_dict[i][gas]['gas_value'])

    avg_trial_time = numpy.mean(trial_time_list)
    avg_difference = numpy.mean(diff_reading_list)
    cal_precision = 100.0 * avg_difference / conc

    pdf_data = {
        'date': date,
        'instrument_id': instrument_id,
        'calib_gas_conc': conc,
        'calib_gas_conc_90': conc_90,
        'trial_time_1': trial_time_list[0],
        'trial_time_2': trial_time_list[1],
        'trial_time_3': trial_time_list[2],
        'avg_trial_time': avg_trial_time,
        'zero_reading_1': zero_gas_meter_reading_list[0],
        'zero_reading_2': zero_gas_meter_reading_list[1],
        'zero_reading_3': zero_gas_meter_reading_list[2],
        'cal_reading_1': cal_gas_meter_reading_list[0],
        'cal_reading_2': cal_gas_meter_reading_list[1],
        'cal_reading_3': cal_gas_meter_reading_list[2],
        'diff_reading_1': diff_reading_list[0],
        'diff_reading_2': diff_reading_list[1],
        'diff_reading_3': diff_reading_list[2],
        'avg_difference': avg_difference,
        'calib_precision': cal_precision,
    }

    # Average Timers for CH4 (90% gas) = (t1 + t2 + t3) / 3
    # Average Concentration Difference = (conc_ch4 - ch4_1) + (conc_ch4 - ch4_2) + (conc_ch4 - ch4_3)) / 3
    # Calib Precision (diff/conc)*100% = ((conc_ch4 - ch4_1) + (conc_ch4 - ch4_2) + (conc_ch4 - ch4_3)) / (3 * conc_ch4) * 100

    return pdf_data


def autocal():
    s, instrument_id = connectBT()
    # file = create_csv()
    # timer = Timer()
    timer = MutableTimer(5)
    # timer = MutableTimer(CAL_TRIAL_TIMER)

    try:
        bufs = BufferedSocket(s)
        print('bufs', bufs)
        temp = bufs.recv(1000000)

        print('Starting Calibration')
        start_timestamp = datetime.now()

        cal_data = {}
        # hat_out_pin = 0     # TODO: Zero Gas
        for trial_number in range(1, NUM_TRIALS+1):
            print('\n\tTrial Number   :', trial_number)

            cal_data[trial_number] = {}
            # Loop twice - ZERO and CH4
            for hat_out_pin in range(NUM_VALVES_ACTIVE):
                gas = VALVE_TO_GAS_MAP[hat_out_pin]
                conc = GAS_CONCENTRATIONS[gas]
                conc_90 = conc * 0.9

                print('\t\tValve Activated    :', hat_out_pin)
                print('\t\tGas on valve       :', gas)
                print('\t\tGas Concentration  :', conc)
                print('\t\tTrial Timer        :', CAL_TRIAL_TIMER, 'seconds\n')

                trial_st_ts = datetime.now()
                curr_trial_time = 0.0
                ppm_ch4 = -1.0

                # timer_flag = True
                # print('timer.flag.get()', timer.flag.get())
                tmp_time = time.time()
                with lock:
                    timer.set(time.time())
                    timer.flag.set(True)
                    # timer.runTimer()

                # automationhat.out[hat_out_pin].on()
                # while timer_flag:
                while timer.flag.get():
                    sensorraws = bufs.recv_until("\n".encode())
                    # print('sensorraws', sensorraws)
                    ppm_ch4 = round(struct.unpack('<f', bytes.fromhex(sensorraws[2:10].decode('utf-8')))[0], 2)
                    ppm_co2 = round(struct.unpack('<f', bytes.fromhex(sensorraws[10:18].decode('utf-8')))[0], 2)
                    # print('ch4', ppm_ch4, '\tco2', ppm_co2, '\ttime: ', time.time() - tmp_time, timer.flag.get(), timer.get(), '__', timer.timer)

                    # if ppm_ch4 >= conc_90 or timer.get() > CAL_TRIAL_TIMER:
                    if ppm_ch4 >= conc_90 or timer.get() >= 5:
                        # print('____\nppm_ch4', ppm_ch4, 'flag', timer.flag.get(), 'timer', timer.get())
                        with lock:
                            # timer_flag = False
                            timer.flag.set(False)
                        curr_trial_time = timer.get()

                # automationhat.out[hat_out_pin].off()
                trial_end_ts = datetime.now()

                cal_data[trial_number][gas] = {
                # tmp_data = {
                    'time_at_gas_90': curr_trial_time,
                    'gas_value': ppm_ch4,
                    'start_timestamp': trial_st_ts,
                    'end_timestamp': trial_end_ts
                }
                print('\n\ncal_data :', cal_data, '\n\n')

                """ End of valve """
            """ End of trial """
        """ Data acquired """
        """
        cal_data = { trial 1 : {...},
                     trial 2 : {...},
                     trial 3 : {...} }
        
        Each trail has = { 'ZERO' : {...},
                           'CH4'  : {...} }
        """

        pdf_data_dict = parseCalData(cal_data, instrument_id)


        # Veh_data = str(date) + ", " + str(time) + ", " + str(ppms) + "," + str(timeit.default_timer() - timer_start) + '\n'
        # file.write(Veh_data)
        # file.flush()


    except Exception as e:
        print('[ EXCEPTION ] ', e)
        traceback.print_exc()

    '''
    file_obj = open('veh_state.json', 'w')
    json.dump(Veh_data, file_obj ,indent = 4)
    '''
    # File object is closed.
    # file_obj.close()
    # file.close()
    s.close()

    # ack = s.send('$STREAM,STOP,*00FF\n')
    # print('Stopping Data Stream\nack: ', ack)
    print('Stopping Data Stream')

    # calib.join()

    # print("\nData write successful, file name:" + str(fileName))
