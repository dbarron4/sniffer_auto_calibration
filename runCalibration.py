"""     Copyright (c) 2018. Treetown Tech LLC     """

#!/usr/bin/env python3

from autocal import *

if __name__ == '__main__':
    autocal()
    #
    # cal_data = {
    #     1: {
    #         'ZERO': {
    #             'time_at_gas_90': 5.123357057571411,
    #             'gas_value': 0.0,
    #             'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 8, 304609),
    #             'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 13, 427966)
    #         },
    #
    #         'CH4': {
    #             'time_at_gas_90': 5.325668811798096,
    #             'gas_value': 0.0,
    #             'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 13, 427966),
    #             'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 18, 753635)
    #         }
    #     },
    #
    #     2: {
    #         'ZERO': {
    #             'time_at_gas_90': 5.338342666625977,
    #             'gas_value': 0.0,
    #             'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 18, 753635),
    #             'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 24, 91978)
    #         },
    #
    #         'CH4': {'time_at_gas_90': 5.009104251861572,
    #                 'gas_value': 0.0,
    #                 'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 24, 91978),
    #                 'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 29, 101082)
    #                 }
    #     },
    #
    #     3: {
    #         'ZERO': {
    #             'time_at_gas_90': 5.00732421875,
    #             'gas_value': 0.0,
    #             'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 29, 101082),
    #             'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 34, 108406)
    #         },
    #
    #         'CH4': {
    #             'time_at_gas_90': 5.328399896621704,
    #             'gas_value': 0.0,
    #             'start_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 34, 108406),
    #             'end_timestamp': datetime.datetime(2019, 4, 5, 9, 41, 39, 436806)
    #         }
    #     }
    # }
    #
    # print(parseCalData(cal_data, '12345'))
