"""     Copyright (c) 2018. Treetown Tech LLC     """

import time
import threading

lock = threading.Lock()


class MutableTimer:
    def __init__(self, timer):
        self.st = 0.0
        self.flag = MutableBool()
        self.timer = timer

    def get(self):
        return time.time() - self.st

    def set(self, cal_start_time=time.time()):
        self.st = cal_start_time

    # def runTimer(self):
    #     with lock:
    #         self.flag.set(True)
    #         self.st = time.time()
    #     while self.flag.get():
    #         if time.time() - self.st >= self.timer:
    #             # with lock:
    #             self.flag.set(False)


class MutableBool:
    def __init__(self, in_val=True):
        self.b = in_val

    def get(self):
        return self.b

    def set(self, in_val):
        self.b = in_val


class Timer:
    def __init__(self):
        self.curr_val = MutableTimer(30)
        self.flag = MutableBool()
