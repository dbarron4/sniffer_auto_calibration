"""     Copyright (c) 2018. Treetown Tech LLC     """

import time
import bluetooth
import traceback

sn_list = {
    0: '92000446',
    1: '92000707',
    2: '92000709',
}

irwin = {
    sn_list[0]: {'mac': '00:07:80:DA:BA:D3', 'port': 1, 'type': 'SXT'},
    sn_list[1]: {'mac': "00:07:80:DA:BA:13", 'port': 1, 'type': 'S'},
    sn_list[2]: {'mac': "00:07:80:DA:BA:4e", 'port': 1, 'type': 'S'}
}


def connectBT():
    sn = '92000707'
    print(irwin[sn])

    try:
        # TODO: Choose 'sn' value automatically
        sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        print(sock)

        mac = irwin[sn]['mac']
        port = irwin[sn]['port']
        sock.connect((mac, port))

        ack = sock.send('$STREAM,STD,BT,*00FF\n')
        print('ack: ', ack)
        if ack == 21:
            print("Connected to", sn)
        time.sleep(2)
        sock.send('\n')

        return sock, sn

    except Exception as e:
        print('Bluetooth Exception')
        traceback.print_exc()
